import React, { useRef, useState } from "react";
import { View, Text, Dimensions, PanResponder, StyleSheet } from "react-native";

const CountMoviment = () => {
    const [cont, setCount] = useState(0);
    const screenHeight = Dimensions.get("window").height;
    const gestureThreshold = screenHeight * 0.25;

    const panResponder = useRef(
        PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onPanResponderMove: (event, gestureState) => {
                // Adicione lógica de movimento aqui se necessário
            },
            onPanResponderRelease: (event, gestureState) => {
                if (gestureState.dy < -gestureThreshold) {
                
                    setCount((prevCount) => prevCount - 1);
                } else if (gestureState.dy > gestureThreshold) {
                    
                    setCount((prevCount) => prevCount + 1);
                }
            },
        })
    ).current;

    return (
        <View
            {...panResponder.panHandlers}
            style={styles.container}
        >
            <Text style={styles.text}>Valor do Contador: {cont}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#f0f0f0", 
    },
    text: {
        fontSize: 20,
        fontWeight: "bold",
        color: "#333", 
    },
});

export default CountMoviment;
