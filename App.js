import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import CountMoviment from "./src/count";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="CountMoviment">
        <Stack.Screen name="CountMoviment" component={CountMoviment} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}




